// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSHUD.h"


void ARTSHUD::DrawHUD() {
	Super::DrawHUD();
	Canvas->SetDrawColor(255, 255, 255, 255);
	if (!Canvas) {
		UE_LOG(RTSLog, Warning, TEXT("Could not find canvas"));
		return;
	}
	FVector2D Min;
	FVector2D Max;
	GetMinMaxSelection(Min, Max);
	if (bDrawSelectionBox) {
		Canvas->K2_DrawBox(Min, Max - Min, 2.0);
	}
	FVector2D TopLeft;
	FVector2D BotRight;
	GetMinMaxSelection(TopLeft, BotRight);

	FVector2D TopRight = FVector2D(BotRight.X, TopLeft.Y);
	FVector2D BotLeft = FVector2D(TopLeft.X, BotRight.Y);
	FVector Direction;
	Canvas->Deproject(TopLeft, TopLeft3D, Direction);
	TopLeft3D += Direction*150.0;
	Canvas->Deproject(TopRight, TopRight3D, Direction);
	TopRight3D += Direction*150.0;
	Canvas->Deproject(BotLeft, BotLeft3D, Direction);
	BotLeft3D += Direction*150.0;
	Canvas->Deproject(BotRight, BotRight3D, Direction);
	BotRight3D += Direction*150.0;
}