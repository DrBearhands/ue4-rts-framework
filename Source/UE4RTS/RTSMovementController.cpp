#include "UE4RTS.h"
#include "RTSMovementController.h"

URTSMovementController::URTSMovementController()
{

}


URTSMovementController::~URTSMovementController()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "GC on RTSMovementController");
}

void URTSMovementController::MoveGroupTo(FVector Location) {
	Destination = Location;
	for (auto Unit : Group) {
		Unit->StartMovement();
	}
}

void URTSMovementController::Tick(float dt) {
	for (auto Unit : Group) {
		if (Unit->State == ESelectableActorState::Moving) {
			URTSMovementController::MoveUnitTo(Unit, Destination, dt);
		}
	}
}

bool URTSMovementController::IsTickable() const {
	return true;
}

TStatId URTSMovementController::GetStatId() const {
	return Super::GetStatID();
}

void URTSMovementController::UnitArrived(ARTSUnit* Unit) {
	RemoveUnitFromGroup(Unit);
}