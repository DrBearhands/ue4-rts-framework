// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RTSSelectableActor.h"
#include "RTSUnit.generated.h"

class URTSMovementController;
/**
 * 
 */
UCLASS()
class UE4RTS_API ARTSUnit : public ARTSSelectableActor
{
	GENERATED_BODY()
	
private:
	friend class URTSMovementController;
//	FVector Destination;
	UPROPERTY()
	URTSMovementController* GroupMovementController = nullptr;
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float MaxSpeed = 100;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Movement")
	FVector Velocity;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Movement")
	FQuat LookQuat;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Movement")
		void OnStartMove();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Movement")
		void OnStopMove();

	ARTSUnit();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void Destroyed();

	virtual void GiveCommand(URTSMovementController* GroupMovementController, ARTSSelectableActor* Target = NULL) override;
	
	FORCEINLINE void StopMovement() {
		State = ESelectableActorState::Idle;
		Velocity.Set(0,0,0);
		OnStopMove();
	}

	FORCEINLINE void StartMovement() {
		State = ESelectableActorState::Moving;
		OnStartMove();
	}
};
