#pragma once

#include "RTSUnit.h"
#include "Tickable.h"
#include "RTSGameMode.h"
#include "RTSMovementController.generated.h"

UCLASS()
class UE4RTS_API URTSMovementController : public UObject, public FTickableGameObject
{
	GENERATED_BODY()

protected:
	TSet<ARTSUnit*> Group;
	FVector Destination;
public:
	URTSMovementController();
	~URTSMovementController();

	FORCEINLINE void AddToGroup(ARTSUnit* NewAddition) {
		Group.Add(NewAddition);
	}

	virtual void MoveGroupTo(FVector Location);

	virtual void Tick(float dt) override;

	virtual bool IsTickable() const override;
	
	virtual TStatId GetStatId() const override;

	virtual void UnitArrived(ARTSUnit* Unit);

	FORCEINLINE void RemoveUnitFromGroup(ARTSUnit* Unit) {
		Unit->StopMovement();
		Group.Remove(Unit);
		Unit->GroupMovementController = nullptr;
	}

	FORCEINLINE void MoveUnitTo(ARTSUnit* Unit, FVector Destination, float DeltaTime) {
		FVector UnitLocation = Unit->GetActorLocation();
		FVector MoveVector = Destination - UnitLocation;
		if (MoveVector.Size() <= 10) {
			UnitArrived(Unit);
		}
		else {
			if (MoveVector.Normalize(1.0)) {
				Unit->LookQuat = MoveVector.ToOrientationQuat();
				Unit->Velocity = MoveVector * Unit->MaxSpeed;
				Unit->SetActorLocation(UnitLocation + Unit->Velocity*DeltaTime, false, NULL, ETeleportType::TeleportPhysics);
				Unit->SetActorRotation(Unit->LookQuat);
			}
			else {
				Unit->StopMovement();
				UE_LOG(RTSLog, Warning, TEXT("Failed to normalize move vector."));
			}
		}
	}
};

