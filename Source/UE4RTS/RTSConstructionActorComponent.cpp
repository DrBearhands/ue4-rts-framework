// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSConstructionActorComponent.h"
#include "RTSSelectableActor.h"


// Sets default values for this component's properties
URTSConstructionActorComponent::URTSConstructionActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URTSConstructionActorComponent::BeginPlay()
{
	Super::BeginPlay();

	ConstructingActor = Cast<ARTSSelectableActor>(GetAttachmentRootActor());
}

void URTSConstructionActorComponent::StartConstruction(FConstructionInfo Info) {
	ConstructedClass = Info.ConstructedClass;
	GetWorld()->GetTimerManager().SetTimer(ConstructionTimer, this, &URTSConstructionActorComponent::FinishConstruction, Info.ConstructionTime, false);
}

void URTSConstructionActorComponent::FinishConstruction() {
	FTransform SpawnTransform = GetComponentTransform();
	ARTSSelectableActor* SpawnedActor = (ARTSSelectableActor*) GetWorld()->SpawnActor(ConstructedClass, &SpawnTransform);
	ConstructingActor->OnFinishedConstruction(this, SpawnedActor);
}