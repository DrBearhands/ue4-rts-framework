// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSConstructionSite.h"

#include "RTSUnit.h"

void ARTSConstructionSite::Tick(float dt) {
	for (auto& Entry : Constructors) {
		ConstructionTime -= Entry.Value*dt;
	}
	if (ConstructionTime <= 0) {
		FinishConstruction();
	}
}

bool ARTSConstructionSite::AddConstructor(ARTSUnit* Constructor, float Modifier) {
	Constructors.Add(Constructor, Modifier);
	return true;
}

bool ARTSConstructionSite::RemoveConstructor(ARTSUnit* Constructor) {
	Constructors.FindAndRemoveChecked(Constructor);
	return true;
}

void ARTSConstructionSite::FinishConstruction() {

}