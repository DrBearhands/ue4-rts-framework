// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSGameMode.h"

#include "RTSPlayerController.h"
#include "RTSPlayerPawn.h"
#include "RTSHUD.h"

//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "debug msg");
DEFINE_LOG_CATEGORY(RTSLog);

ARTSGameMode::ARTSGameMode() {
	DefaultPawnClass = ARTSPlayerPawn::StaticClass();
	PlayerControllerClass = ARTSPlayerController::StaticClass();
	HUDClass = ARTSHUD::StaticClass();
}