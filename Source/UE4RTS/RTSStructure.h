// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RTSSelectableActor.h"
#include "RTSStructure.generated.h"

/**
 * 
 */
UCLASS()
class UE4RTS_API ARTSStructure : public ARTSSelectableActor
{
	GENERATED_BODY()

	virtual void BeginPlay() override;
	
	
};
