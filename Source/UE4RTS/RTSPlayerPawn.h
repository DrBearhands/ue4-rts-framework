// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "RTSPlayerPawn.generated.h"

UCLASS(Blueprintable)
class UE4RTS_API ARTSPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	USpringArmComponent* SpringArmComponent;
	UCameraComponent* CameraComponent;

	// Sets default values for this pawn's properties
	ARTSPlayerPawn();
};
