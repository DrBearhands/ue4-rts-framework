// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSPlayerPawn.h"
#include "RTSPlayerController.h"

// Sets default values
ARTSPlayerPawn::ARTSPlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ARTSPlayerController* Player = Cast<ARTSPlayerController>(ARTSPlayerController::StaticClass()->GetDefaultObject());
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera spring arm"));
	SpringArmComponent->TargetArmLength = Player->CameraDistance;
	SpringArmComponent->RelativeRotation = FRotator(-Player->CameraAngle, 0.f, 0.f);
	SpringArmComponent->bDoCollisionTest = false;
	RootComponent = SpringArmComponent;
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
}