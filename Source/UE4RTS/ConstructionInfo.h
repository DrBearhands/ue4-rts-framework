// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RTSSelectableActor.h"
#include "ConstructionInfo.generated.h"
/**
 * 
 */
USTRUCT(BlueprintType)
struct UE4RTS_API FConstructionInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly)
	float ConstructionTime;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ARTSSelectableActor> ConstructedClass;
};
