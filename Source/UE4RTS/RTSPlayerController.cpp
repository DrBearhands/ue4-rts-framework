// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSSelectableActor.h"
#include "RTSPlayerController.h"
#include "RTSHUD.h"
#include "RTSPlayerPawn.h"
#include "RTSNavMeshMovementController.h"

ARTSPlayerController::ARTSPlayerController() {
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

void ARTSPlayerController::BeginPlay() {
	RTSHUD = Cast<ARTSHUD>(GetHUD());
}

void ARTSPlayerController::Tick(float dt) {
	float MouseX, MouseY;
	GetMousePosition(MouseX, MouseY);
	//Update HUD mouse info
	RTSHUD->SelectionEnd.X = MouseX;
	RTSHUD->SelectionEnd.Y = MouseY;

	//Update camera/pawn location based on mouse position (pan at borders)
	const FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	MouseX_ClipSpace = MouseX / ViewportSize.X;
	MouseY_ClipSpace = MouseY / ViewportSize.Y;

	APawn* Pawn = GetPawn();
	if (MouseX_ClipSpace >= 0.99 || IsInputKeyDown(EKeys::D)) {
		Pawn->AddActorWorldOffset(FVector(0, 1, 0)*PanSpeed, false);
	}
	else if (MouseX_ClipSpace <= 0.01 || IsInputKeyDown(EKeys::A)) {
		Pawn->AddActorWorldOffset(FVector(0, -1, 0)*PanSpeed, false);
	}
	if (MouseY_ClipSpace >= 0.99 || IsInputKeyDown(EKeys::S)) {
		Pawn->AddActorWorldOffset(FVector(-1, 0, 0)*PanSpeed, false);
	}
	else if (MouseY_ClipSpace <= 0.01 || IsInputKeyDown(EKeys::W)) {
		Pawn->AddActorWorldOffset(FVector(1, 0, 0)*PanSpeed, false);
	}
}

void ARTSPlayerController::NotifySelectableDestroyed(ARTSSelectableActor* Deceased)
{
	Selection.Remove(Deceased);
}

void ARTSPlayerController::AddToSelection(ARTSSelectableActor* NewlySelected) {
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, "selected " + NewlySelected->GetName());
	NewlySelected->SelectingPlayers.Add(this);
	Selection.Add(NewlySelected);
}

void ARTSPlayerController::ClearSelection() {
	for (auto Selected : Selection) {
		Selected->SelectingPlayers.Remove(this);
	}
	Selection.Empty();
}

void ARTSPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	InputComponent->BindKey(EKeys::LeftMouseButton, IE_Pressed, this, &ARTSPlayerController::LeftMousePressed);
	InputComponent->BindKey(EKeys::LeftMouseButton, IE_Released, this, &ARTSPlayerController::LeftMouseReleased);
	InputComponent->BindKey(EKeys::RightMouseButton, IE_Pressed, this, &ARTSPlayerController::RightMousePressed);
	InputComponent->BindKey(EKeys::RightMouseButton, IE_Released, this, &ARTSPlayerController::RightMouseReleased);
}

void ARTSPlayerController::LeftMousePressed() {
	RTSHUD->bDrawSelectionBox = true;
	GetMousePosition(RTSHUD->SelectionStart.X, RTSHUD->SelectionStart.Y);
}

void ARTSPlayerController::LeftMouseReleased() {
	if (!IsInputKeyDown(MultiSelectKey))
		ClearSelection();
	
	RTSHUD->bDrawSelectionBox = false;
	if (!WasMouseDragged()) {
		FHitResult TraceResult(ForceInit);
		GetHitResultUnderCursor(ECollisionChannel::ECC_WorldDynamic, false, TraceResult);
		ARTSSelectableActor* Selected = Cast<ARTSSelectableActor>(TraceResult.GetActor());
		if (Selected) {
			Selection.Add(Selected);
		}
	}
	else {
		ApplyToSelectableInDragBox<ARTSPlayerController, &ARTSPlayerController::AddToSelection>(*this);
	}
}

void ARTSPlayerController::RightMousePressed() {
	FHitResult TraceResult(ForceInit);
	GetHitResultUnderCursor(ECollisionChannel::ECC_WorldDynamic, false, TraceResult);
	FVector Location = TraceResult.Location;
	ARTSSelectableActor* Target = Cast<ARTSSelectableActor>(TraceResult.GetActor());
	UPROPERTY() URTSMovementController* GroupMovementController = NewObject<URTSNavMeshMovementController>();
	for (auto Selected : Selection) {
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "moving " + Selected->GetName() + " to " + Location.ToString());
		Selected->GiveCommand(GroupMovementController, Target);
	}
	GroupMovementController->MoveGroupTo(Location);
}


void ARTSPlayerController::RightMouseReleased() {
	//GEngine->GameViewport->Viewport->LockMouseToViewport(true);
}
