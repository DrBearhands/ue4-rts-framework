// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSUnit.h"
#include "RTSGameMode.h"
#include "RTSMovementController.h"

void ARTSUnit::GiveCommand(URTSMovementController* GroupMovementController, ARTSSelectableActor* Target) {
	if (this->GroupMovementController) {
		this->GroupMovementController->RemoveUnitFromGroup(this);
	}
	this->GroupMovementController = GroupMovementController;
	GroupMovementController->AddToGroup(this);
}

ARTSUnit::ARTSUnit() {
	PrimaryActorTick.bCanEverTick = true;
}

void ARTSUnit::BeginPlay() {
	Super::Super::BeginPlay();
	LookQuat = GetActorQuat();
}

void ARTSUnit::Destroyed() {
	Super::Destroyed();
}


// Called every frame
void ARTSUnit::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
}