// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RTSStructure.h"
#include "RTSConstructionSite.generated.h"

class ARTSUnit;
/**
 * 
 */
UCLASS()
class UE4RTS_API ARTSConstructionSite : public ARTSStructure
{
	GENERATED_BODY()

	float ConstructionTime;
	TSubclassOf<ARTSSelectableActor> ConstructedActor;

	TMap<ARTSUnit*, float> Constructors;

	virtual void Tick(float dt) override;

	virtual void FinishConstruction();
	
	virtual bool AddConstructor(ARTSUnit* Constructor, float Modifier);

	virtual bool RemoveConstructor(ARTSUnit* Constructor);
};
