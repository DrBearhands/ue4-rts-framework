// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RTSSelectableActor.generated.h"

UENUM(BlueprintType)
enum class ESelectableActorState : uint8 {
	Idle		UMETA(DisplayName = "Idle"),
	Moving		UMETA(DisplayName = "Moving")
};

class ARTSPlayerController;
class URTSConstructionActorComponent;
class URTSMovementController;

UCLASS(abstract)
class UE4RTS_API ARTSSelectableActor : public AActor
{
	GENERATED_BODY()
	
private:
	TSet<ARTSPlayerController*> SelectingPlayers;

	FTimerHandle ConstructionTimer;
public:
	friend class ARTSPlayerController;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTSSelectableActor")
	ESelectableActorState State = ESelectableActorState::Idle;

	// Sets default values for this actor's properties
	ARTSSelectableActor();

	virtual void Destroyed() override;

	UFUNCTION(BlueprintCallable, Category = "Actions")
	virtual void GiveCommand(URTSMovementController* GroupMovementController, ARTSSelectableActor* Target = NULL);

	virtual void OnFinishedConstruction(URTSConstructionActorComponent* ConstructingComponent, ARTSSelectableActor* ConstructedActor);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Construction", DisplayName = "OnFinishConstruction")
	void BP_FinishConstruction(URTSConstructionActorComponent* ConstructingComponent, ARTSSelectableActor* ConstructedActor);
};
