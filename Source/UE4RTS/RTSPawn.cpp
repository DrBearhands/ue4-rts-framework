// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSPawn.h"


// Sets default values
ARTSPawn::ARTSPawn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARTSPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARTSPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

