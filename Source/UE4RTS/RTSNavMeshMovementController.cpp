#include "UE4RTS.h"
#include "RTSNavMeshMovementController.h"


URTSNavMeshMovementController::URTSNavMeshMovementController()
{
}


URTSNavMeshMovementController::~URTSNavMeshMovementController()
{
}

/*
void URTSNavMeshMovementController::TeleportOntoNavMesh() {
	UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(GetWorld());
	if (NavSys)
	{
		FNavAgentProperties AgentProperties = FNavAgentProperties(10, 10);
		ANavigationData* NavData = NavSys->GetNavDataForProps(AgentProperties);
		if (NavData) {
			FVector FoundLocation = NavSys->ProjectPointToNavigation(
				GetWorld(),
				GetActorLocation(),
				NavData,
				UNavigationQueryFilter::StaticClass(),
				FVector(300, 300, 300)
			);
			GEngine->AddOnScreenDebugMessage(-1, 100.0f, FColor::Red, GetName() + " teleporting unto navmesh at " + FoundLocation.ToString());
			SetActorLocation(FoundLocation);
		}
	}
}
*/

void URTSNavMeshMovementController::MoveGroupTo(FVector Location) {
	Super::MoveGroupTo(Location);
	PlanPaths(Location);
}

void URTSNavMeshMovementController::UnitArrived(ARTSUnit* Unit) {
	UnitNavigationData& NavData = UnitsNavigationInfo[Unit];
	if (++NavData.DestinationNavPointIdx == NavData.NumNavPoints) { //Final destination
		RemoveUnitFromGroup(Unit);
	}
}

void URTSNavMeshMovementController::Tick(float DeltaSeconds) {
	for (auto Unit : Group) {
		UE_LOG(RTSLog, Log, TEXT("Moving unit in group Tick"));
		if (Unit->State == ESelectableActorState::Moving) {
			UnitNavigationData& NavData = UnitsNavigationInfo[Unit];
			const TArray<FNavPathPoint>& PathPoints = NavData.Path->GetPathPoints();
			FVector CurrentSubDestination = PathPoints[NavData.DestinationNavPointIdx].Location;
			MoveUnitTo(Unit, CurrentSubDestination, DeltaSeconds);
		}
	}
}