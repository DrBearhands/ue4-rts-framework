// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4RTS.h"
#include "RTSSelectableActor.h"
#include "RTSPlayerController.h"


// Sets default values
ARTSSelectableActor::ARTSSelectableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*UStaticMeshComponent* SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetCanEverAffectNavigation(false);
	RootComponent = SphereVisual;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/BasicShapes/Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		SphereVisual->SetWorldScale3D(FVector(1.0f));
	}*/
}

void ARTSSelectableActor::Destroyed() {
	Super::Destroyed();
	for (auto SelectingPlayer : SelectingPlayers) {
		SelectingPlayer->NotifySelectableDestroyed(this);
	}
}

void ARTSSelectableActor::GiveCommand(URTSMovementController* GroupMovementController, ARTSSelectableActor* Target) {
	//Override
}

void ARTSSelectableActor::OnFinishedConstruction(URTSConstructionActorComponent* ConstructingComponent, ARTSSelectableActor* ConstructedActor) {
	BP_FinishConstruction(ConstructingComponent, ConstructedActor);
}