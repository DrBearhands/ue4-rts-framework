#pragma once
#include "RTSMovementController.h"
#include "Navigation/PathFollowingComponent.h"
#include "RTSNavMeshMovementController.generated.h"


struct UnitNavigationData {
	FNavPathSharedPtr Path;
	uint8 DestinationNavPointIdx;
	uint8 NumNavPoints;
};

UCLASS()
class UE4RTS_API URTSNavMeshMovementController :
	public URTSMovementController
{
	GENERATED_BODY()

protected:
	TMap<ARTSUnit*, UnitNavigationData> UnitsNavigationInfo;
public:
	URTSNavMeshMovementController();
	~URTSNavMeshMovementController();

	virtual void MoveGroupTo(FVector Location);
	virtual void Tick(float) override;

	virtual void UnitArrived(ARTSUnit* Unit) override;

	FORCEINLINE bool PlanPath(ARTSUnit* Unit, FVector Start, FVector Destination, UnitNavigationData& UnitNavData) {
		UE_LOG(RTSLog, Log, TEXT("Getting NavSys"));
		UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(Unit->GetWorld());
		if (NavSys)
		{
			UE_LOG(RTSLog, Log, TEXT("Got NavSys"));
			//from AAIController::PreparePathfinding
			FNavAgentProperties AgentProperties = FNavAgentProperties(10, 10);
			ANavigationData* NavData = NavSys->GetNavDataForProps(AgentProperties);
			if (NavData) {
				UE_LOG(RTSLog, Log, TEXT("Got NavData"));
				FSharedConstNavQueryFilter NavFilter = UNavigationQueryFilter::GetQueryFilter(*NavData, this, UNavigationQueryFilter::StaticClass());
				FPathFindingQuery Query = FPathFindingQuery(this, *NavData, Start, Destination, NavFilter);

				//from AAIController::RequestPathAndMove
				FPathFindingResult PathResult = NavSys->FindPathSync(Query);
				if (PathResult.Result != ENavigationQueryResult::Error)
				{
					UE_LOG(RTSLog, Log, TEXT("Got pathplanning result"));
					if (PathResult.IsSuccessful() && PathResult.Path.IsValid())
					{
						UE_LOG(RTSLog, Log, TEXT("PlanPath successful"));
						/*if (MoveRequest.IsMoveToActorRequest())
						{
						PathResult.Path->SetGoalActorObservation(*MoveRequest.GetGoalActor(), 100.0f);
						}*/

						PathResult.Path->EnableRecalculationOnInvalidation(true);

						UnitNavData.Path = PathResult.Path;
						UnitNavData.NumNavPoints = UnitNavData.Path->GetPathPoints().Num();
						UnitNavData.DestinationNavPointIdx = 0;
						return true;
					}
				}
			}
		}
		return false;
	}

	FORCEINLINE void PlanPaths(FVector Destination) {
		UnitsNavigationInfo.Empty();
		for (auto Unit : Group) {
			UE_LOG(RTSLog, Log, TEXT("Getting path for unit"));
			UnitNavigationData NavData;
			if (PlanPath(Unit, Unit->GetActorLocation(), Destination, NavData)) {
				UnitsNavigationInfo.Add(Unit, NavData);
				UE_LOG(RTSLog, Log, TEXT("Got a path"));
			}
			
		}
		UE_LOG(RTSLog, Log, TEXT("Got all paths"));
	}
};

