// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "RTSGameMode.h"
#include "RTSHUD.generated.h"

class ARTSPlayerController;
/**
 * 
 */
UCLASS()
class UE4RTS_API ARTSHUD : public AHUD
{
	GENERATED_BODY()

protected:
	FVector2D SelectionStart;
	FVector2D SelectionEnd;
	bool bDrawSelectionBox = false;

	FVector TopLeft3D, TopRight3D, BotLeft3D, BotRight3D;
public:
	FORCEINLINE void GetMinMaxSelection(FVector2D& Min, FVector2D& Max) {
		Min = FVector2D(FMath::Min(SelectionStart.X, SelectionEnd.X), FMath::Min(SelectionStart.Y, SelectionEnd.Y));
		Max = FVector2D(FMath::Max(SelectionStart.X, SelectionEnd.X), FMath::Max(SelectionStart.Y, SelectionEnd.Y));
	}

	FORCEINLINE TArray<FPlane> GetSelectionBoundingPlanes(FVector CameraLocation) {
		TArray<FPlane> BoundingPlanes;

		//NB.: Order of vectors important
		BoundingPlanes.Add(FPlane(CameraLocation, TopRight3D, TopLeft3D)); //Upper plane
		BoundingPlanes.Add(FPlane(CameraLocation, BotLeft3D, BotRight3D)); //Lower plane
		BoundingPlanes.Add(FPlane(CameraLocation, BotRight3D, TopRight3D)); //Right plane
		BoundingPlanes.Add(FPlane(CameraLocation, TopLeft3D, BotLeft3D)); //Left plane
		return BoundingPlanes;
	}
	friend class ARTSPlayerController;
	virtual void DrawHUD() override;
};
