// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "RTSGameMode.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(RTSLog, Log, All);

/**
 * 
 */
UCLASS()
class UE4RTS_API ARTSGameMode : public AGameMode
{
	GENERATED_BODY()
	ARTSGameMode();
};
