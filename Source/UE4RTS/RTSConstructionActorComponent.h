// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "ConstructionInfo.h"
#include "RTSConstructionActorComponent.generated.h"

class ARTSSelectableActor;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UE4RTS_API URTSConstructionActorComponent : public USceneComponent
{
	GENERATED_BODY()

	FTimerHandle						ConstructionTimer;
	TSubclassOf<ARTSSelectableActor>	ConstructedClass;
	ARTSSelectableActor*				ConstructingActor;
public:	

	UPROPERTY(EditDefaultsOnly)
	int ID;
	// Sets default values for this component's properties
	URTSConstructionActorComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	//virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Construction", DisplayName = "StartConstruction")
	void StartConstruction(FConstructionInfo Cinfo);
	
	void FinishConstruction();
};
