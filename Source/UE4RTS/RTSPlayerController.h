// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "RTSPlayerPawn.h"
#include "RTSHUD.h"
#include "RTSSelectableActor.h"
#include "RTSPlayerController.generated.h"
/**
 * 
 */

class ARTSSelectableActor;
class ARTSHUD;

UCLASS()
class UE4RTS_API ARTSPlayerController : public APlayerController
{
	GENERATED_BODY()

		ARTSPlayerController();

private:
	TSet<ARTSSelectableActor*> Selection;

	void NotifySelectableDestroyed(ARTSSelectableActor*);
protected:
	ARTSHUD* RTSHUD;
public:
	friend class ARTSSelectableActor;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RTSPlayerController")
		FKey MultiSelectKey = EKeys::LeftControl;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Settings")
		float PanSpeed = 5.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Settings")
		float CameraDistance = 400.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Settings")
		float CameraAngle = 60.f;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Mouse Control")
		float MouseX_ClipSpace;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Mouse Control")
		float MouseY_ClipSpace;

	UPROPERTY()
		TArray<URTSMovementController*> GroupMovementControllers;

	virtual void BeginPlay() override;
	virtual void Tick(float) override;
	void AddToSelection(ARTSSelectableActor*);
	void ClearSelection();
	
	virtual void SetupInputComponent() override;

	void LeftMousePressed();
	void LeftMouseReleased();

	void RightMousePressed();
	void RightMouseReleased();

	/**
	Used to apply a function to all actors within selection box bounds
	*/
	template<typename ObjClass, void (ObjClass::*MemberFunction)(ARTSSelectableActor*)>
	FORCEINLINE void ApplyToSelectableInDragBox(ObjClass& Caller) {
		FVector CamLocation = Cast<ARTSPlayerPawn>(GetPawn())->CameraComponent->GetComponentLocation();

		TArray<FPlane> BoundingPlanes = RTSHUD->GetSelectionBoundingPlanes(CamLocation);
		for (TActorIterator<ARTSSelectableActor> Iterator(GetWorld()); Iterator; ++Iterator)
		{
			// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
			ARTSSelectableActor* Selectable = *Iterator;
			FVector Location = Iterator->GetActorLocation();
			float Radius = 0;/* Iterator->GetSimpleCollisionRadius();*/
			bool bIsInAllPlanes = true;
			for (const auto& Plane : BoundingPlanes) {
				float PlaneDistance = Plane.PlaneDot(Location);
				if (PlaneDistance < -Radius) {
					bIsInAllPlanes = false;
					break;
				}
			}
			if (bIsInAllPlanes) {
				(Caller.*MemberFunction)(Selectable);
			}
		}
	}

	FORCEINLINE bool WasMouseDragged() {
		float MouseX, MouseY;
		GetMousePosition(MouseX, MouseY);
		return RTSHUD->SelectionStart.X != MouseX && RTSHUD->SelectionStart.Y != MouseY;
	}

//	virtual bool InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad) override;
};
